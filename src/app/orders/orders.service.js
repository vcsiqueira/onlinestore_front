(function () {
    'use strict';

    angular.module('app.orders')
        .factory('ordersSrv', ordersSrv);

    ordersSrv.$inject = ['$http', '$q', 'constants'];

    function ordersSrv($http, $q, constants) {

        return {
            getAll: getAll,
            addCart: addCart,
            buy: buy,
            cancel: cancel,
        };

        function getAll() {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/cart")
                .then(function success(request) {
                    deferred.resolve(request.data.response);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;
        }

        function addCart(data) {
            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + "/cart/addProduct", data)
                .then(function success(request) {
                    deferred.resolve(request.data.response);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;
        }

        function cancel(id) {
            var deferred = $q.defer();
            $http.put(constants.apiBaseUrl + "/cart/cancel/" + id, {})
                .then(function success(request) {
                    deferred.resolve(request.data.response);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;
        }

        function buy(data){
            var deferred = $q.defer();
            $http.put(constants.apiBaseUrl + "/cart/buy/" + data.id, data)
                .then(function success(request) {
                    deferred.resolve(request.data.response);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;

        }

    }
})();