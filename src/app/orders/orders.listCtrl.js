(function () {
    'use strict';

    angular.module('app.orders')
            .controller('ordersListCtrl', ordersListCtrl);

    ordersListCtrl.$inject = ['header', 'ordersSrv', 'Flash', 'ngDialog', '$scope'];

    function ordersListCtrl(header, ordersSrv, Flash, ngDialog, $scope) {
        var vm = this;
        vm.orders = [];
        vm.isLoaded = false;
        vm.showCheckoutForm = showCheckoutForm;
        vm.sumTotal = sumTotal;
        vm.checkout = checkout;
        vm.orderSelected = null;
        vm.cancel = cancel;

        activate();

        function showCheckoutForm(order) {

            vm.orderSelected = order;

            ngDialog.open({
                template: "app/orders/_checkoutForm.html",
                className: 'ngdialog-theme-default custom-width',
                showClose: false,
                closeByDocument: false,
                closeByEscape: false,
                scope: $scope
            });
        }

        function sumTotal(products){
            var total = 0;
            angular.forEach(products, function(product){
                total += parseFloat(product.price);
            });

            return total;

        }

        function cancel(id){
            vm.isLoaded = true;
            return ordersSrv.cancel(id).then(function (data) {
                console.log("DATA", data);

                if (data.order === true) Flash.showSuccess("Order canceled successfully !");
                else Flash.showError("Erro on cancel order !");

            }).finally(function () {
                vm.isLoaded = true;
                activate();
            });
        }

        function checkout(){
            console.log(vm.orderSelected);

            vm.isLoaded = true;
            return ordersSrv.buy(vm.orderSelected).then(function (data) {
                console.log("DATA", data);

                if (data.order === true) Flash.showSuccess("Order paid successfully !");
                else Flash.showError("Payment not authorized !");

            }).finally(function () {
                vm.isLoaded = true;
                activate();
            });
        }




        function activate() {
            vm.isLoaded = false;
            header.title = 'Orders';
            header.description = 'List';
            header.text = 'Área de administração dos produtos';

            return ordersSrv.getAll().then(function (data) {

                console.log("DATA", data);
                vm.orders = data.orders;
                return vm.products;
            }).finally(function () {
                vm.isLoaded = true;
            });

        }

    }

})();