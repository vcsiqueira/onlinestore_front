(function () {
    'use strict';
    angular.module('app.orders', [
        //'ngRoute',
        'app',
        'app.core',
        'app.header'
    ]).config(config);
    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
                .when('/orders', {
                    templateUrl: 'app/orders/orders.list.html',
                    controller: 'ordersListCtrl',
                    controllerAs: 'list'
                }).otherwise({
            // Redireciono para a home em caso de rota errada
            redirectTo: '/'
        });
    }
})();
