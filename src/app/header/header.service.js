(function () {
    'use strict';

    angular
            .module('app.header')
            .factory('header', header);

    function header() {
        
        var factory = {
            title : 'Header Empty',
            description : 'Description Empty',
            breadcrumbs : 'Breadcrumbs Empty',
            text : 'Text Empty'
        };
        
        return factory;
    }

})();