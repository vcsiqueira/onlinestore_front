(function () {
    'use strict';
    angular.module('app.main')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = [ 'header', '$location', 'authSrv', 'Flash', 'menuSrv', 'mainSrv', '$scope','$window'];

    function MainCtrl(header, $location, authSrv, Flash, menuSrv, mainSrv, $scope, $window) {

        var vm = this;
        vm.header = header;
        vm.init = init;
        vm.logout = authSrv.logout;
        vm.isLoggedIn = authSrv.checkisLoggedIn;
        vm.getFlashMessage = Flash.get;
        vm.getFlashHeader = Flash.getHeader;
        vm.hasFlashMessage = Flash.hasMessage;
        vm.getFlashClass = Flash.getClass;
        vm.getFlashIcon = Flash.getIcon;
        vm.trocaSenha = trocaSenha;
        vm.getUserName = authSrv.getAuthUserName;
        vm.menu = menuSrv;
        vm.fixNgMenu = fixNgMenu;
        vm.ativandoMenu = ativandoMenu;
        vm.ativandoHeader = ativandoHeader;
        vm.ativabox = ativabox;

        vm.init();

        function init() {
            // Header da página
            header.title = 'Home';
            header.description = '';
            header.text = 'Área principal com mapa de vistorias e gráficos';
            // Recuperando o menu
            // menuSrv.setMenu();

            // Inicializando o AuthService
            authSrv.init();
            // Redireciona para o Auth caso não esteja logado
            if (!authSrv.isLoggedIn && $location.path() === '/') {
                $location.path('/auth');
            }

            ativabox();

        }

        function trocaSenha() {
            $location.path('/usuario/trocasenha');
        }




        function fixNgMenu(evento) {

            var elem = angular.element(evento.currentTarget);
            var checkElement = elem.next();

            //if this isn't a link, prevent the page from being redirected
            if (checkElement.is('.treeview-menu') && elem.parent("li").is('.treeview')) {
                evento.preventDefault();
            }

            //Check if the next element is a menu and is visible
            if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible'))) {
                //Close the menu
                checkElement.slideUp('normal', function () {
                    checkElement.removeClass('menu-open');
                    //Fix the layout in case the sidebar stretches over the height of the window
                    //_this.layout.fix();
                });
                checkElement.parent("li").removeClass("active");
            }
            //If the menu is not visible
            else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
                //Get the parent menu
                var parent = elem.parents('ul').first();
                //Close all open menus within the parent
                var ul = parent.find('ul:visible').slideUp('normal');
                //Remove the menu-open class from the parent
                ul.removeClass('menu-open');
                //Get the parent li
                var parent_li = elem.parent("li");

                //Open the target menu and add the menu-open class
                checkElement.slideDown('normal', function () {
                    //Add the class active to the parent li
                    checkElement.addClass('menu-open');
                    parent.find('li.active').removeClass('active');
                    parent_li.addClass('active');
                    //Fix the layout in case the sidebar stretches over the height of the window
                    var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
                    var window_height = $(window).height();
                    var sidebar_height = $('.sidebar').height();
                    //Set the min-height of the content and sidebar based on the
                    //the height of the document.
                    if ($('body').hasClass('fixed')) {
                        $('.content-wrapper, .right-side').css('min-height', window_height - $('.main-footer').outerHeight());
                    } else {
                        if (window_height >= sidebar_height) {
                            $('.content-wrapper, .right-side').css('min-height', window_height - neg);
                        } else {
                            $('.content-wrapper, .right-side').css('min-height', sidebar_height);
                        }
                    }
                });
            }
        }

        function ativandoMenu() {
            $.AdminLTE.tree('.sidebar');
        }

        function ativandoHeader() {
            $.AdminLTE.pushMenu.activate("[data-toggle='offcanvas']");
        }

        function ativabox() {
            $.AdminLTE.boxWidget.activate();
        }

    }
})();

