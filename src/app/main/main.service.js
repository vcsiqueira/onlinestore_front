(function () {
    'use strict';

    angular.module('app.main')
        .factory('mainSrv', mainSrv);

    mainSrv.$inject = ['$http', '$q', 'constants', '$routeParams'];

    function mainSrv($http, $q, constants, $routeParams) {

        return {
            getAll: getAll,
            getQtdMes: getQtdMes,
            getQtdUsuario: getQtdUsuario,
        };

        function getAll() {
            var deferred = $q.defer();
            // $http.get(constants.apiBaseUrl + "/header?orderBy=header.id&sortedBy=desc&filter=header.id;usuario.nome;latitude;longitude;header.dt_fim&count=-1")
            $http.get(constants.apiBaseUrl + "/header?orderBy=header.id&sortedBy=desc&count=-1")
                .then(function success(request) {                    
                    deferred.resolve(request.data.response);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;            
        }
        
        function getQtdMes() {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/dashboard/header/qtdmes")
                .then(function success(request) {                    
                    deferred.resolve(request.data.response.data);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;             
        }
                
        function getQtdUsuario() {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/dashboard/header/qtdusuario")
                .then(function success(request) {                    
                    deferred.resolve(request.data.response.data);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;             
        }
                
    }
})();/**
 * Created by vcsiqueira on 09/12/15.
 */
