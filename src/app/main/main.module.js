(function () {
    'use strict';

    angular.module('app.main', [
        
        // Angular modules
        'ngRoute',
        
        // Custom modules
        'app',
        'app.auth',
        // 3rd part modules
        //'angular-jwt',
        //'ui-router',
        //'ngDialog'
        
    ]).config(config);

    config.$inject = ['$routeProvider'];
    function config($routeProvider) {


        $routeProvider
                .when('/', {
                    templateUrl: 'app/main/main.html',
                    controller: 'MainCtrl',
                    controllerAs: 'ctrl'
                });
        
    }

})();