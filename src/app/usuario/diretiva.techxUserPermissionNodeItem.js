(function() {
    'use strict';

    angular.module('app.usuario').directive('techxUserPermissionNodeItem', [
        '$compile',
        function($compile) {

            var nodeItemDir = '<span ng-bind-html="node.descricao" ng-class="{\'text-danger\': node.status == \'I\'}"></span>&nbsp; ' +
                    '<div style="display: inline" ng-show="node.tipo == \'dir\' && ctrl.tree.data.selected == node.id && ctrl.tree.data.descricao == node.descricao">' +
                    '</div>';

            var nodeItemFolder = '<span ng-bind-html="node.descricao" ng-class="{\'text-danger\': node.status == \'I\'}"></span>&nbsp; ' +
                    '<div style="display: inline" ng-show="node.tipo == \'form\'"> ' +
                    '<a href="javascript:;" data-ng-click="ctrl.toggleStatusPermissionForm(node)" ng-show="node.status_permissao === \'P\'" title="Desativar"><span class="fa-stack fa-sm"><i class="fa fa-folder-o" style="color: #d2d6de !important"></i><i class="fa fa-toggle-on fa-stack-1x"></i></span></a>' +
                    '<a href="javascript:;" data-ng-click="ctrl.toggleStatusPermissionForm(node)" ng-show="node.status_permissao === \'N\'" title="Ativar"><span class="fa-stack fa-sm"><i class="fa fa-folder-o" style="color: #d2d6de !important"></i><i class="fa fa-toggle-off fa-stack-1x"></i></span></a>' +
                    '</div>';

            return {
                restrict: 'E',
                link: function(scope, element, attrs) {
                    if (scope.node.tipo == 'dir') {
                        element.replaceWith($compile(nodeItemDir)(scope));
                    } else {
                        element.replaceWith($compile(nodeItemFolder)(scope));
                    }
                }
            };
        }
    ]);

})();