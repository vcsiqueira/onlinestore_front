(function() {
    'use strict';

    angular.module('app.usuario').controller('usuarioEditCtrl', [
        '$q',
        '$scope',
        '$routeParams',
        '$location',
        'Flash',
        'constants',
        'header',
        'usuarioSrv',
        'empresaSrv',
        'formularioSrv',
        'acessoPermissionSrv',
        function($q, $scope, $routeParams, $location, Flash, constants, header, usuarioSrv, empresaSrv, formularioSrv, acessoPermissionSrv) {

            var self = this;
            self.main = {
                execute: getChangeRequest,
                combo: {
                    formulario: []
                },
                tree: {
                    data: {
                        selected: null,
                        descricao: null
                    },
                    opts: {},
                    treecontrol: {
                        "expandedNodes": []
                    }
                }
            };
            /**
             * Método especifico para a diretiva tree
             *
             * @param {type} node
             * @param {type} selected
             * @returns {undefined}
             */
            self.showSelected = function(node, selected) {
                self.main.tree.data.selected = null;
                self.main.tree.data.descricao = null;
                if (selected) {
                    self.main.tree.data.selected = node.id;
                    self.main.tree.data.descricao = node.descricao;
                }
            };

            self.toggleStatusPermissionForm = function(node) {
                if (node.status_permissao === "P") {
                    var status = "N";
                    node.status_permissao = "N";
                } else if (node.status_permissao === "N") {
                    var status = "P";
                    node.status_permissao = "P";
                }
                vm.isLoaded = false;
                usuarioSrv.giveUsserPermissionOnForm(vm.usuario.id, node.id, status).finally(function() {
                    vm.isLoaded = true;
                });
            };

            self.tab = function(id, e) {
                e.preventDefault();
                $(id).tab('show');
            };

            /**
             * Metodo que gerencia todas as requisições do lista.
             * Como filtros e detalhamento de click nas listas.
             *
             * @param {string} origin parâmetro opcional que informa a origem da requisição
             *  que a função respondera de acordo.
             * @returns {.$q@call;defer.promise}
             */
            function getChangeRequest(origin) {
                var deferred = $q.defer(), formulario;

                switch (origin) {
                    default:
                        formulario = formularioSrv.getByIdProjetoAndUsuario(vm.usuario.id).then(function(data) {
                            self.main.combo.formulario = data.formulario;
                            $scope.gridOptions.data = data.formulario;
                        });
                }

                vm.isLoaded = false;

                $q.all([formulario]).then(function() {
                    deferred.resolve();
                }).catch(function(e) {
                    Flash.showError('Erro ao carregar a listagem.');
//                    console.log("Log de error: ", e);
                    deferred.reject();
                }).finally(function() {
                    vm.isLoaded = true;
                });

                return deferred.promise;
            }

            $scope.gridOptions = {
                appScopeProvider: self,
                paginationPageSizes: [10, 25, 50, 75, 100],
                paginationPageSize: 10,
                enableRowSelection: true,
                enableRowHeaderSelection: false,
                multiSelect: false,
                enableFiltering: true,
                useExternalFiltering: false,
                useExternalPagination: false,
                useExternalSorting: false,
                data: [],
                // Grid menu
                enableGridMenu: true,
                //gridMenuTitleFilter: fakeI18n,
                columnDefs: [
                    {field: 'status_permissao', width: 25, displayName: '#', enableFiltering: false, enableSorting: false,
                        cellTemplate: '<input type="checkbox" ng-click="grid.appScope.toggleStatusPermissionForm(row.entity)" ng-model="row.entity.status_check">'
                    },
                    {field: 'id', displayName: 'ID', width: 80, operation: 'like'},
                    {field: 'descricao', displayName: 'Descrição', width: 300, operation: 'like'},
                    {field: 'diretorio', displayName: 'Dieretórios', width: 200, operation: 'like'},
                    {field: 'projeto', displayName: 'Projetos', width: 200, operation: 'like'},
                    {field: 'cliente', displayName: 'Clientes', width: 200, operation: 'like'}
                ],
                // Indicará qual operação realizar no server ao enviar uma consulta.
                columnOperations: {
                    "id": "=",
                    "descricao": "like",
                    "diretorio": "like",
                    "projeto": "like",
                    "cliente": "like"
                },
                onRegisterApi: function(gridApi) {
                    $scope.gridApi = gridApi;
                }
            };


            var vm = this;
            //vm.usuario = {};
            vm.save = save;
            vm.papeis = null;
            vm.cancel = back;
            vm.empresa = [];
            vm.permissions = [];
            vm.permissions_selected = [];
            vm.roles_selected = [];
            vm.isLoaded = false;
            vm.status = [
                {id: "A", desc: "Ativo"},
                {id: "I", desc: "Inativo"}
            ];

            vm.listPermissionsAccessExists = listPermissionsAccessExists;
            vm.listPermissionsAccessToggle = listPermissionsAccessToggle;

            vm.title = {
                icon: "fa-pencil",
                label: "Edição"
            };

            vm.action = "edit";

            activate();

            function activate() {

                header.title = 'Usuário';
                header.description = 'Cadastro';

                var roles_medicao = acessoPermissionSrv.getRolesMedicao().then(function success(response) {
//                            console.log("Roles");
                    vm.roles = angular.fromJson(response.data);
                }).catch(function error(response) {
                    Flash.showError('Erro ao recuperar os dados do cliente.');
                });

                var perm = acessoPermissionSrv.getAllPermissionsAccess().then(function success(response) {
                    vm.permissions = response.response.data;
                });

                var pap = usuarioSrv.getPapeis().then(function success(response) {
                    vm.papeis = response.papeis;
                });

                // Caregando a listagem de epresas
                var emp = empresaSrv.getAll().then(function success(response) {
                    vm.empresa = response.empresa;
                    vm.empresa.unshift({"id": "", "descricao": "Escolha um item..."});
                });

                // Recuperando o usuário
                var user = usuarioSrv.getById($routeParams.id).then(function success(response) {
                    vm.usuario = response.usuario;
                    vm.permissions_selected = response.permissions;
                    vm.roles_selected = (response.usuario.roles_medicao !== null) ? angular.fromJson(response.usuario.roles_medicao) : [];
                }).catch(function error(response) {
                    Flash.showError('Erro ao recuperar o registro.');
                });

                $q.all([pap, emp, user, perm, roles_medicao]).then(function() {
                    vm.isLoaded = true;
                }).finally(function() {
                    // Initial state of aplication
                    getChangeRequest();
                });
            }

            function back() {
                redirect();
            }

            function save() {

                vm.usuario.roles_medicao = JSON.stringify(vm.roles_selected);
                vm.isLoaded = false;
                usuarioSrv.edit(vm.usuario, vm.permissions_selected).then(function success(response) {
                    Flash.showSuccess('Registro alterado.');
                    redirect();
                }).catch(function error(response) {
                    Flash.showError('Erro ao alterar o registro.');
                }).finally(function(response) {
                    vm.isLoaded = true;
                });
            }

            function redirect() {
                clear();
                $location.path('/usuario');
            }

            // Reseta os dados do usuário
            function clear() {
                vm.usuario = {};
            }

            function listPermissionsAccessExists(item, list) {
//                console.log("listPermissionsAccessExists");
                return list.indexOf(item) > -1;
            }

            function listPermissionsAccessToggle(item, list) {
//                console.log("listPermissionsAccessToggle");
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                } else {
                    list.push(item);
                }
            }
        }
    ]);
})();