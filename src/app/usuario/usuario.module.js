(function() {
    'use strict';
    angular.module('app.usuario', [
        'ngRoute',
        'app',
        'app.core',
    ]).config(config);
    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
                .when('/usuario', {
                    templateUrl: 'app/usuario/usuario.list.html',
                    controller: 'usuarioListCtrl',
                    controllerAs: 'list'
                })
                .when('/usuario/trocasenha', {
                    templateUrl: 'app/usuario/usuario.trocaSenhaForm.html',
                    controller: 'usuarioTrocaSenhaCtrl',
                    controllerAs: 'ctrl'
                })
                .when('/usuario/add', {
                    templateUrl: 'app/usuario/usuario.form.html',
                    controller: 'usuarioAddCtrl',
                    controllerAs: 'ctrl'
                })
                .when('/usuario/edit/:id', {
                    templateUrl: 'app/usuario/usuario.form.html',
                    controller: 'usuarioEditCtrl',
                    controllerAs: 'ctrl'
                }).otherwise({
            // Redireciono para a home em caso de rota errada
            redirectTo: '/'
        });
    }
})();
