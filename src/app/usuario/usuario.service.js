(function() {
    'use strict';

    angular.module('app.usuario')
            .factory('usuarioSrv', usuarioSrv);

    usuarioSrv.$inject = ['$http', '$q', 'constants'];

    function usuarioSrv($http, $q, constants) {

        return {
            add: add,
            edit: edit,
            remove: remove,
            getAll: getAll,
            getById: getById,
            trocarSenha: trocarSenha,
            sendMessage: sendMessage,
            resetPass: resetPass,
            getPapeis: getPapeis,
            giveUsserPermissionOnForm: giveUsserPermissionOnForm
        };

        function add(data, permission_selected) {
            data["permission_selected"] = null;
            data["permission_selected"] = permission_selected;
            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + "/usuario", data)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function edit(data, permission_selected) {
            data["permission_selected"] = null;
            data["permission_selected"] = permission_selected;
            var deferred = $q.defer();
            $http.put(constants.apiBaseUrl + "/usuario/" + data.id, data)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function remove(data) {
            var deferred = $q.defer();
            $http.delete(constants.apiBaseUrl + "/usuario/" + data.id)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function resetPass(data) {
            var deferred = $q.defer();

            $http.get(constants.apiBaseUrl + "/usuario/resetpass/" + data.id)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response || "Erro inesperado na api.");
                    });
            return deferred.promise;
        }

        function getAll(param) {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/usuario", param)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function getById(id) {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/usuario/" + id)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function trocarSenha(data) {
            var deferred = $q.defer();
            $http.put(constants.apiBaseUrl + "/usuario/" + data.id + "/trocarsenha", data)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function sendMessage(data) {
            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + "/usuario/gcm/send", data)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function getPapeis() {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/papeis")
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        function giveUsserPermissionOnForm(usuario_id, formulario_id, status_permissao) {
            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + "/usuario/PermForm", {
                usuario_id: usuario_id,
                formulario_id: formulario_id,
                status: status_permissao
            }).then(function(request) {
                deferred.resolve(request.data.response);
            }).catch(function(request) {
                deferred.reject(request.data.response);
            });
            return deferred.promise;
        }
    }
})();