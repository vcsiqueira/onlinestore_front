(function () {
    'use strict';

    angular.module('app.usuario')
            .controller('usuarioTrocaSenhaCtrl', usuarioTrocaSenhaCtrl);

    usuarioTrocaSenhaCtrl.$inject = ['header', 'usuarioSrv', '$location', 'Flash', 'authSrv'];

    function usuarioTrocaSenhaCtrl(header, usuarioSrv, $location, Flash, authSrv) {

        var vm = this;
        vm.usuario = {};
        vm.save = save;
        vm.cancel = back;
        vm.isLoaded = true;

        vm.title = {
            icon: "fa-pencil",
            label: "Alteração de senha"
        };

        activate();

        function activate() {

            header.title = 'Usuário';
            header.description = 'Trocar Senha';

        }

        function back() {
            redirect();
        }

        function save() {
            vm.isLoaded = false;
            vm.usuario.id = authSrv.getAuthUserId();

            usuarioSrv.trocarSenha(vm.usuario)
                    .then(function success(response) {
                        Flash.showSuccess('Registro alterado.');
                        redirect();
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao alterar o registro.');
                    })
                    .finally(function (response) {
                        vm.isLoaded = true;
                        vm.usuario.id = null;
                    });
        }

        function redirect() {
            clear();
            $location.path('/');
        }

        // Reseta os dados do usuário
        function clear() {
            vm.usuario = {};
        }

    }

})();