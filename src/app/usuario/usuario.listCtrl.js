(function () {
    'use strict';

    angular.module('app.usuario')
            .controller('usuarioListCtrl', usuarioListCtrl);

    usuarioListCtrl.$inject = ['header', 'usuarioSrv', '$location', 'Flash', 'ngDialog', '$scope', 'uiGridConstants', 'i18nService', 'uiGridFilterService', '$interval', 'authSrv'];

    function usuarioListCtrl(header, usuarioSrv, $location, Flash, ngDialog, $scope, uiGridConstants, i18nService, uiGridFilterService, $interval, authSrv) {
        var vm = this;
        vm.usuario = [];
        vm.add = add;
        vm.edit = edit;
        vm.remove = remove;
        vm.resetPass = resetPass;
        vm.getAuthUserName = authSrv.getAuthUserName();
        uiGridFilterService.setInstance(vm.constructor.name);
        vm.userPushNotificationList = [];
        activate();

        function add() {
            $location.path('/usuario/add');
        }

        function edit(obj) {
            $location.path('/usuario/edit/' + obj.id);
        }

        function remove(item) {

            ngDialog.openConfirm({
                template: "app/core/core.confirma.exclusao.html",
                className: 'ngdialog-theme-default custom-width',
                showClose: false,
                closeByDocument: false,
                closeByEscape: false,
                scope: $scope
            }).then(function (data) {
                delItem(item);
            });

        }
        
        function resetPass(item) {

            ngDialog.openConfirm({
                template: "app/usuario/confirma.alteracao.senha.html",
                className: 'ngdialog-theme-default custom-width',
                showClose: false,
                closeByDocument: false,
                closeByEscape: false,
                scope: $scope
            }).then(function (data) {
                resetItem(item);
            });

        }

        function delItem(data) {

            usuarioSrv.remove(data)
                    .then(function success(response) {
                        getGridData(uiGridFilterService.getParamItems());
                        Flash.showSuccess("Usuário " + data.nome + " excluido com sucesso.");
                        vm.group = null;
                    })
                    .catch(function error(response) {
                        Flash.showError("Erro ao excluir o usuário " + data.nome);
                    });

        }
        
        function resetItem(data) {

            usuarioSrv.resetPass(data)
                    .then(function success(response) {
                        Flash.showSuccess("A senha do usuário " + data.nome + " foi alterada com sucesso.");
                        vm.group = null;
                    })
                    .catch(function error(response) {
                        Flash.showError("Erro ao alterar a senha do usuário " + data.nome);
                    });

        }

        function activate() {

            header.title = 'Usuário';
            header.description = 'Listagem';
            header.text = 'Área de administração dos usuários do sistema';

            i18nService.setCurrentLang('pt');
            getGridData(uiGridFilterService.getParamItems());
        }

        // UI-GRID
        vm.getGridData = getGridData;

        // Implementações para o ui-grid
        var paginationGridOptions = {
            pageNumber: 1,
            pageSize: 10,
            sort: null
        };

        //$scope.state = {};
        $scope.gridOptions = {
            appScopeProvider: vm,
            paginationPageSizes: [10, 25, 50, 75, 100],
            paginationPageSize: 10,
            enableRowSelection: false,
            enableRowHeaderSelection: true,
            multiSelect: true,
            enableFiltering: true,
            useExternalFiltering: true,
            useExternalPagination: true,
            useExternalSorting: true,
            data: [],
            // Grid menu
            enableGridMenu: true,
            //gridMenuTitleFilter: fakeI18n,
            columnDefs: [
                {field: 'public|usuario-id', displayName: 'ID', enableSorting: true, width: 50},
                {field: 'public|empresa-descricao', displayName: 'Descrição', width: 200, enableFiltering: false, enableSorting: false},
                {field: 'public|usuario-nome', displayName: 'Nome', width: 300},
                {field: 'public|usuario-email', displayName: 'Email', width: 250},
                {field: 'public|usuario-username', displayName: 'Usuário', width: 150},
                {field: 'regional', displayName: 'Regional', width: 100},
                {field: 'public|usuario-status', displayName: 'Status', width: 50},
                {field: 'public|usuario-type_user', displayName: 'Tipo de Usuário', width: 150},
                {field: 'public|usuario-roles_medicao', displayName: 'Roles', width: 150},
                {field: 'public|usuario-master_editor', displayName: 'M. Editor', width: 150}
            ],
            // Indicará qual operação realizar no server ao enviar uma consulta.
            columnOperations: {
                "public|usuario-id": "=",
                "public|empresa-descricao": "like",
                "public|usuario-nome": "like",
                "public|usuario-email": "like",
                "public|usuario-username": "like",
                "regional": "like",
                "public|usuario-status": "like",
                "public|usuario-type_user": "like",
                "public|usuario-roles_medicao": "like",
                "public|usuario-master_editor": "like"
            },
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;

                // Adicionando a coluna das ações
                var cellTemplate = 'app/usuario/uiGrid_list_tpl.html';
                $scope.gridApi.core.addRowHeaderColumn({name: 'rowHeaderCol', displayName: 'Ação', width: 65, cellTemplate: cellTemplate});

                // Realizando o sort
                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {

                    if (sortColumns.length === 0) {
                        paginationGridOptions.sort = null;
                        uiGridFilterService.setSortBy(null, null);
                    } else {
                        paginationGridOptions.sort = sortColumns[0].sort.direction;

                        uiGridFilterService.setSortBy(sortColumns[0].name, sortColumns[0].sort.direction);
                    }
                    getGridData(uiGridFilterService.getParamItems());

                });

                // Realizando a busca pela nova página
                gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {

                    uiGridFilterService.setPageAndTotal(newPage, pageSize);
                    getGridData(uiGridFilterService.getParamItems());

                });

                gridApi.selection.on.rowSelectionChanged($scope, function (row) {

                    var userIdList = [];
                    angular.forEach(vm.userPushNotificationList, function(item) {
                        userIdList.push(item.id);
                    });
                    
                    var idx = userIdList.indexOf(row.entity.id);
                    if(userIdList.length === 0 && row.isSelected) {
                        vm.userPushNotificationList.push(row.entity);
                    } else if(row.isSelected && idx === -1) {
                        vm.userPushNotificationList.push(row.entity);
                    } else if(!row.isSelected && idx !== -1) {
                        vm.userPushNotificationList.splice(idx,1);
                    }
                });
                
                gridApi.selection.on.rowSelectionChangedBatch($scope, function (rows) {

                    var userIdList = [];
                    angular.forEach(vm.userPushNotificationList, function(item) {
                        userIdList.push(item.id);
                    });
                    
                    var i = 0;
                    for(i = rows.length-1; i >= 0; i--){
                        var idx = userIdList.indexOf(rows[i].entity.id);
                        if(userIdList.length === 0 && rows[i].isSelected) {
                            vm.userPushNotificationList.push(rows[i].entity);
                        } else if(rows[i].isSelected && idx === -1) {
                            vm.userPushNotificationList.push(rows[i].entity);
                        } else if(!rows[i].isSelected && idx !== -1) {
                            vm.userPushNotificationList.splice(i,1);
                        }
                    }
                    
                });
                
            }
        };
        
        function selectDataOnGrid() {
            angular.forEach(vm.userPushNotificationList, function(user) {
                
                angular.forEach($scope.gridOptions.data, function(each) {
                   
                    if(each.id === user.id) {
                        $scope.gridApi.selection.selectRow(each);
                    }
                });
                
            });
        }

        //## Criando o header da tabela ##
        $scope.getGridData = getGridData;
        function getGridData(params) {
            vm.isLoaded = false;
            usuarioSrv.getAll(params)
                    .then(function success(response) {
                        $scope.gridOptions.data = response.usuario.data;
                        $scope.gridOptions.totalItems = response.usuario.total;
                        $scope.gridOptions.paginationCurrentPage = parseInt(response.usuario.current_page);
                        $scope.gridOptions.paginationPageSize = parseInt(response.usuario.per_page);

                        vm.usuario = response.usuario.data;
                        //$interval( function() {ttt;}, 0, 2);
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao recuperar os dados.');
                    })
                    .finally(function () {
                        vm.isLoaded = true;
                        $interval( function() {selectDataOnGrid();}, 0, 1);
                        $scope.gridApi.saveState.restore($scope, uiGridFilterService.state);
                        
                    });
        }
        
        vm.showSendMessageBox = showSendMessageBox;
        function showSendMessageBox() {
            
            vm.mensagem = "";
            if(vm.userPushNotificationList.length === 0) {
                Flash.showAlert('Selecione usuários na grid para enviar uma mensagem.');
                return false;
            }
            
            ngDialog.open({
                template: "app/usuario/showUserMessageBox.html",
                className: 'ngdialog-theme-default custom-width',
                showClose: false,
                closeByDocument: false,
                closeByEscape: false,
                scope: $scope
            });
        }
        
        vm.sendMessage = sendMessage;
        function sendMessage() {
            
            if(vm.userPushNotificationList.length === 0 || vm.mensagem === "") {
                return false;
            }
            
            var idList = [];
            angular.forEach(vm.userPushNotificationList, function(item) {
                idList.push(item.id);
            });
            
            var body = {
                "usuarios": idList,
                "mensagem": {"texto" : vm.mensagem, "titulo": "FiscalWEB"}
            };
            
            usuarioSrv.sendMessage(body)
                    .then(function success(response) {
                        vm.isLoaded = false;
                    })
                    .catch(function error(response) {
                        Flash.showError('Erro ao enviar a mensagem.');
                    })
                    .finally(function () {
                        vm.isLoaded = true;
                        Flash.showSuccess('Mensagem enviada com sucesso.');
                        ngDialog.closeAll();
                        
                    });
        }

    }

})();