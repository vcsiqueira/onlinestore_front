(function() {
    'use strict';
    angular.module('app.usuario').controller('usuarioAddCtrl', [
        '$q',
        '$location',
        'Flash',
        'header',
        'usuarioSrv',
        'empresaSrv',
        'acessoPermissionSrv',
        function($q, $location, Flash, header, usuarioSrv, empresaSrv, acessoPermissionSrv) {
            var vm = this;
            vm.usuario = {};
            vm.papeis = [];
            vm.save = save;
            vm.cancel = back;
            vm.empresa = [];
            vm.permissions = [];
            vm.permissions_selected = [];
            vm.roles = null;
            vm.roles_selected = [];
            vm.listPermissionsAccessExists = listPermissionsAccessExists;
            vm.listPermissionsAccessToggle = listPermissionsAccessToggle;
            vm.isLoaded = false;
            vm.status = [
                {id: "A", desc: "Ativo"},
                {id: "I", desc: "Inativo"}
            ];
            vm.title = {
                icon: "fa-file-o",
                label: "Inclusão"
            };

            vm.tab = function(id, e) {
                e.preventDefault();
                $(id).tab('show');
            };

            activate();
            function activate() {
                header.title = 'Usuário';
                header.description = 'Cadastro';

                var roles_medicao = acessoPermissionSrv.getRolesMedicao().then(function success(response) {
//                  console.log("Roles");
                    vm.roles = angular.fromJson(response.data);
                    angular.forEach(vm.roles, function(role) {
                        vm.roles_selected.push(role.desc);
                    });
                }).catch(function error(response) {
                    Flash.showError('Erro ao recuperar os dados do cliente.');
                });
                var perm = acessoPermissionSrv.getAllPermissionsAccess().then(function success(response) {
                    vm.permissions = response.response.data;
                });
                var pap = usuarioSrv.getPapeis().then(function success(response) {
                    vm.papeis = response.papeis;
                });
                // Caregando a listagem de epresas
                var emp = empresaSrv.getAll().then(function success(response) {
                    vm.empresa = response.empresa;
                    vm.empresa.unshift({"id": "", "descricao": "Escolha um item..."});
                });

                $q.all([perm, pap, emp, , roles_medicao]).then(function() {
                    vm.isLoaded = true;
                    vm.usuario.status = 'A';
                });
            }

            function back() {
                redirect();
            }

            function save() {
                vm.isLoaded = false;
                vm.usuario.roles_medicao = JSON.stringify(vm.roles_selected);
                usuarioSrv.add(vm.usuario, vm.permissions_selected).then(function success(response) {
                    Flash.showSuccess('Registro criado.');
                    redirect();
                }).catch(function error(response) {
                    Flash.showError('Erro ao criar o registro.');
                }).finally(function(response) {
                    vm.isLoaded = true;
                });
            }

            function redirect() {
                clear();
                $location.path('/usuario');
            }

            // Reseta os dados do usuário
            function clear() {
                vm.usuario = {};
            }

            function listPermissionsAccessExists(item, list) {
                //console.log("listSegmentoExists");
                return list.indexOf(item) > -1;
            }

            function listPermissionsAccessToggle(item, list) {
                //console.log("listSegmentoToggle");
                var idx = list.indexOf(item);
                if (idx > -1) {
                    list.splice(idx, 1);
                } else {
                    list.push(item);
                }
            }
        }
    ]);
})();