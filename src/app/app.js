(function() {
    'use strict';

    angular.module('app', [
        // Angular modules
        'ngSanitize',
        'ngRoute',
        // Custom modules
        'app.core',
        'app.directives',
        'app.main',
        'app.header',
        'app.usuario',
        'app.auth',
        'app.products',
        'app.orders',
        // 3rd part modules
        //'angular-jwt',
        'ngTasty',
        'ui.grid',
        'ui.grid.autoResize',
        'ui.grid.pagination',
        'ui.grid.resizeColumns',
        'ui.grid.pinning',
        'ui.grid.cellNav',
        'ui.grid.saveState',
        'ui.grid.selection',
        'ui.grid.exporter',
        'ui.grid.moveColumns',
        'ui.grid.grouping',
        'ngDialog',
        'ngVis',
        'angular-toArrayFilter',
        'ngAnimate',
        'ui.bootstrap',
        'ngStorage',
        'ui.utils.masks',
        'localytics.directives',
    ]).config([
        '$httpProvider',
        'chosenProvider',
        function($httpProvider, chosenProvider) {

            $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
            $httpProvider.defaults.useXDomain = true;
            delete $httpProvider.defaults.headers.common['X-Requested-With'];

            chosenProvider.setOption({
                width: '100%'
            });
        }
    ]);
})();