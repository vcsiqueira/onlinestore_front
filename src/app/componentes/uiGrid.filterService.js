(function () {
    'use strict';

    angular.module('app.core')
            .service('uiGridFilterService', uiGridFilterService);
    //uiGridFilterService.$inject = [''];

    function uiGridFilterService() {

        var srvList = {};
        var conf = {
            // Parâmetros de ordenação
            "sortedBy": null,
            "orderBy": null,
            // Parâmetros de paginação
            "page": 1,
            "count": 10,
            // Parâmetros de filtro
            "filter": null,
            // Parâmetros de filtro por conteúdo
            "curinga": null,
            // Estado da grid
            "state": {}
        };
        var instanceName = null;

        return {
            setSortBy: setSortBy,
            setPageAndTotal: setPageAndTotal,
            setFilterTerm: setFilterTerm,
            getFilterTerm: getFilterTerm,
            setCuringaTerm: setCuringaTerm,
            getParamItems: getParamItems,
            getCurrentPage: getCurrentPage,
            getTotalPerPage: getTotalPerPage,
            setState: setState,
            getState: getState,
            setInstance: setInstance
        };

        function setInstance(name) {

            instanceName = name;

            if (!srvList.hasOwnProperty(name)) {
                // Criando o novo item
                srvList[instanceName] = angular.copy(conf);
            }

        }

        function setSortBy(field, order) {
            srvList[instanceName]["orderBy"] = field;
            srvList[instanceName]["sortedBy"] = order;
        }

        function setPageAndTotal(page, totalPerPage) {
            srvList[instanceName].page = page;
            srvList[instanceName].count = totalPerPage;
        }

        function setFilterTerm(filter) {
            srvList[instanceName].filter = filter;
        }

        function getFilterTerm() {
            return srvList[instanceName].filter;
        }

        function setCuringaTerm(curinga) {
            srvList[instanceName].curinga = curinga;
        }

        function getParamItems() {

            var paramItems = {
                params: {}
            };

            if (instanceName === null) {
                return angular.copy(conf);
            }

            paramItems.params['sortedBy'] = srvList[instanceName]['sortedBy'];
            paramItems.params['orderBy'] = srvList[instanceName]['orderBy'];
            paramItems.params.page = srvList[instanceName].page;
            paramItems.params.count = srvList[instanceName].count;

            // Montando os filtros
            angular.forEach(srvList[instanceName].filter, function (value, key) {
                paramItems.params[key] = value;
            });

            // Montando os filtros curinga
            angular.forEach(srvList[instanceName].curinga, function (value, key) {
                paramItems.params[key] = value;
            });

            return paramItems;

        }

        function getCurrentPage() {
            return srvList[instanceName].page;
        }

        function getTotalPerPage() {
            return srvList[instanceName].count;
        }

        function setState(value) {
            srvList[instanceName].state = value;
        }

        function getState() {
            return srvList[instanceName].state;
        }
    }
})();