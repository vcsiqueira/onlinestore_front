(function () {
    'use strict';
    angular.module('app.products', [
        //'ngRoute',
        'app',
        'app.core',
        'app.header'
    ]).config(config);
    config.$inject = ['$routeProvider'];
    function config($routeProvider) {
        $routeProvider
                .when('/products', {
                    templateUrl: 'app/products/products.list.html',
                    controller: 'productsListCtrl',
                    controllerAs: 'list'
                }).otherwise({
            // Redireciono para a home em caso de rota errada
            redirectTo: '/'
        });
    }
})();
