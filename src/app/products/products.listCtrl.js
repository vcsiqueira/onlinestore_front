(function () {
    'use strict';

    angular.module('app.products')
            .controller('productsListCtrl', productsListCtrl);

    productsListCtrl.$inject = ['header', 'productsSrv','ordersSrv', '$location', 'Flash', 'ngDialog', '$scope'];

    function productsListCtrl(header, productsSrv, ordersSrv, $location, Flash, ngDialog, $scope) {
        var vm = this;
        vm.products = [];
        vm.isLoaded = false;
        vm.addProduct = addProduct;

        activate();

        function addProduct(prod_id) {
            vm.isLoaded = false;
            console.log(prod_id);
            return ordersSrv.addCart({"product_id":prod_id}).then(function (data) {

                if (data.orders === true) Flash.showSuccess("Product added successfully !");
                else Flash.showError("Error on add product on order !");
            }).finally(function () {
                vm.isLoaded = true;
            });

        }

        function activate() {

            vm.isLoaded = false;

            header.title = 'Produtos';
            header.description = 'Listagem';
            header.text = 'Área de administração dos produtos';

            return productsSrv.getAll().then(function (data) {

                console.log("DATA", data);
                vm.products = data.products;
                return vm.products;
            }).finally(function () {
                vm.isLoaded = true;
            });

        }

    }

})();