(function () {
    'use strict';

    angular.module('app.products')
        .factory('productsSrv', productsSrv);

    productsSrv.$inject = ['$http', '$q', 'constants'];

    function productsSrv($http, $q, constants) {

        return {
            getAll: getAll,
        };

        function getAll() {
            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/products")
                .then(function success(request) {
                    deferred.resolve(request.data.response);
                })
                .catch(function error(request) {
                    deferred.reject(request.data.response);
                });
            return deferred.promise;
        }

    }
})();