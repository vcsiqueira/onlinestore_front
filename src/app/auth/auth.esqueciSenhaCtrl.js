(function () {
    'use strict';

    angular.module('app.auth')
            .controller('authEsqueciSenhaCtrl', authEsqueciSenhaCtrl);

    authEsqueciSenhaCtrl.$injector = ['authSrv', '$location', 'Flash', 'header'];

    function authEsqueciSenhaCtrl(authSrv, $location, Flash, header) {
        var vm = this;
        vm.email = null;
        vm.enviar = enviar;
        vm.cancelar = redirect;
        vm.init = init;
        vm.isLoaded = true;

        vm.init();

        function enviar() {
            vm.isLoaded = false;
            authSrv.redefinirSenha({email:vm.email})
                    .then(function success(response) {
                        Flash.showSuccess('Solicitação enviada com sucesso.');
                        redirect();
                    }, function error(response) {
                        Flash.showError(response.message);
                    }).finally(function () {
                vm.isLoaded = true;
            });
        }

        function redirect() {
            $location.path('/login');
        }

        function init() {
            // Header da página
            header.title = '';
            header.description = '';

            authSrv.init();
            if (authSrv.isLoggedIn) {
                redirect();
            }
        }

    }

})();