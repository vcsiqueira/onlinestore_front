(function () {
    'use strict';

    angular.module('app.auth')
            .factory('authSrv', authSrv);

    authSrv.$inject = ['$http', 'constants', '$window', '$location', '$q', 'menuSrv'];

    function authSrv($http, constants, $window, $location, $q, menuSrv) {

        var factory = {
            isLoggedIn: false,
            checkisLoggedIn: checkisLoggedIn,
            getToken: getToken,
            login: login,
            loginAuto: loginAuto,
            logout: logout,
            init: init,
            getAuthUserName: getAuthUserName,
            getAuthUserEmpresaId: getAuthUserEmpresaId,
            getAuthUserId: getAuthUserId,
            redefinirSenha: redefinirSenha
        };

        function init() {
            factory.isLoggedIn = checkisLoggedIn();
        }

        function checkisLoggedIn() {
            return ($window.sessionStorage.token != 'undefined' && $window.sessionStorage.token != null) || false;
        }

        function getToken() {
            return $window.sessionStorage.token;
        }

        function login(user) {

            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + '/login', user)
                    .then(success)
                    .catch(fail);

            function success(request) {
                // Criando os dados na Sessão
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.usuario;
                delete $window.sessionStorage.menu;
                $window.sessionStorage.token = request.data.response.token;
                $window.sessionStorage.usuario = angular.toJson(request.data.response.usuario);

                $window.sessionStorage.menu = angular.toJson(request.data.response.menu);
                menuSrv.menu = request.data.response.menu;
                
                init();
                deferred.resolve(true);
            }

            function fail(request) {
                // Logando os dados
                init();
                deferred.reject(request.data.response || {message: "Erro inesperado no servidor."});
            }
            // Retornando a promise
            return deferred.promise;
        }


        function loginAuto(token) {

            var deferred = $q.defer();
            $http.get(constants.apiBaseUrl + "/loginAuto",{'params': {'token': token}})
                    .then(success)
                    .catch(fail);

            function success(request) {
                // Criando os dados na Sessão
                delete $window.sessionStorage.token;
                delete $window.sessionStorage.usuario;
                delete $window.sessionStorage.menu;
                $window.sessionStorage.token = request.data.response.token;
                $window.sessionStorage.usuario = angular.toJson(request.data.response.usuario);

                $window.sessionStorage.menu = angular.toJson(request.data.response.menu);
                menuSrv.menu = request.data.response.menu;

                init();
                deferred.resolve(true);
            }

            function fail(request) {
                // Logando os dados
                init();
                deferred.reject(request.data.response || {message: "Erro inesperado no servidor."});
            }
            // Retornando a promise
            return deferred.promise;
        }

        function logout() {
            factory.isLoggedIn = false;
            delete $window.sessionStorage.token;
            delete $window.sessionStorage.diretorio;
            delete $window.sessionStorage.usuario;
            delete $window.sessionStorage.menu;
            $location.path('/login');
        }

        function saveAuthUser(user) {
            $window.sessionStorage.usuario = angular.toJson(user);
        }

        function getAuthUserName() {
            try {
                var retorno = ' --- ';
                var user = angular.fromJson($window.sessionStorage.usuario);
                retorno = user.nome;
                
            } catch (e) {
                // Faço nada
            } finally {
                return retorno;
            }

        }

        function getAuthUserEmpresaId() {
            try {
                var retorno = ' --- ';
                var user = angular.fromJson($window.sessionStorage.usuario);
                retorno = user.empresa_id;

            } catch (e) {
                // Faço nada
            } finally {
                return retorno;
            }

        }

        function getAuthUserId() {
            
            try {
                var retorno = 'unknown';
                var user = angular.fromJson($window.sessionStorage.usuario);
                retorno = user.id;
                
            } catch (e) {
                // Faço nada
            } finally {
                return retorno;
            }
        }
        
        function redefinirSenha(data) {
            var deferred = $q.defer();
            $http.post(constants.apiBaseUrl + "/redefinirSenha", data)
                    .then(function success(request) {
                        deferred.resolve(request.data.response);
                    })
                    .catch(function error(request) {
                        deferred.reject(request.data.response);
                    });
            return deferred.promise;
        }

        return factory;
    }
})();