(function () {
    'use strict';
    
    angular.module('app.auth',[
        'app',
        'app.core'
    ]).config(config);
    config.$inject = ['$routeProvider', '$httpProvider'];
    function config($routeProvider,$httpProvider) {
        
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.headers.common = 'Content-Type: application/json';
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        
        $routeProvider
                .when('/auth', {
                    templateUrl: 'app/auth/auth.login.html',
                    controller: 'authLoginCtrl',
                    controllerAs: 'authCtrl'
                })
                .when('/auth_auto', {
                    templateUrl: 'app/auth/auth.login.auto.html',
                    controller: 'authLoginAutoCtrl',
                })
                .when('/auth/esquecisenha', {
                    templateUrl: 'app/auth/auth.esqueciSenha.html',
                    controller: 'authEsqueciSenhaCtrl',
                    controllerAs: 'authCtrl'
                });

    }
})();