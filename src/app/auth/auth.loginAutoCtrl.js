(function () {
    'use strict';

    angular.module('app.auth')
            .controller('authLoginAutoCtrl', authLoginAutoCtrl);

    authLoginAutoCtrl.$inject = ['authSrv', '$location', 'Flash'];

    function authLoginAutoCtrl(authSrv, $location, Flash) {
        var vm = this;
        vm.token = {};
        vm.init = init;
        vm.isLoaded = true;

        vm.init();

        function init() {
            vm.isLoaded = false;
            authSrv.loginAuto($location.search().token)
                    .then(function success(response){
                        Flash.showSuccess('Logado com sucesso.');
                        redirect();
                    },function error(response){
                        Flash.showError(response.message);
                    }).finally(function(){
                        vm.isLoaded = true;
                    });
        }
        
        function redirect() {
            delete $location.$$search.token;
            $location.path('/');
        }
        
    }

})();