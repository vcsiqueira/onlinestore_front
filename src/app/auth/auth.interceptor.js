(function () {

    'use strict';

    angular.module('app.auth')
            .factory('authinterceptor', authinterceptor);

    authinterceptor.$inject = ['$q', '$location', '$window'];

    function authinterceptor($q, $location, $window) {

        return {
                    'request': function (config) {
                        config.headers = config.headers || {};
                        if ($window.sessionStorage.token) {
                            config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                        }
                        return config;
                    },
                    'responseError': function (response) {

                        console.log('erro');
                        if (response.status === 401) {
                            delete $window.sessionStorage.token;
                            delete $window.sessionStorage.diretorio;
                            $location.path('/auth');
                        }
                        return $q.reject(response);
                    }
                };
    }

    angular.module('app.auth').config(config);
    config.$inject = ['$httpProvider'];
    function config ($httpProvider) {
        
        $httpProvider.interceptors.push('authinterceptor');
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        
    };


})();