(function () {
    'use strict';

    angular.module('app.auth')
            .controller('authLoginCtrl', authLoginCtrl);

    authLoginCtrl.$inject = ['authSrv', '$location', 'Flash', 'header'];

    function authLoginCtrl(authSrv, $location, Flash, header) {
        var vm = this;
        vm.user = {};
        vm.login = login;
        vm.init = init;
        vm.isLoaded = true;
        vm.solicitarSenha = solicitarSenha;

        vm.init();

        function login() {
            vm.isLoaded = false;
            authSrv.login(vm.user)
                    .then(function success(response){
                        Flash.showSuccess('Logado com sucesso.');
                        redirect();
                    },function error(response){
                        Flash.showError(response.message);
                    }).finally(function(){
                        vm.isLoaded = true;
                    });
        }
        
        function redirect() {
            $location.path('/');
        }
        
        function init() {
            // Header da página
            header.title = '';
            header.description = '';
            
            authSrv.init();
            if (authSrv.isLoggedIn) {
                redirect();
            }
        }
        
        function solicitarSenha() {
            $location.path('/auth/esquecisenha');
        }
    }

})();