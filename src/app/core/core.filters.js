(function () {
    'use strict';
    angular.module('app.core')
            .filter('capitalize', function () {
                return function (input, scope) {
                    if (input != null)
                        return input.substring(0, 1).toUpperCase() + input.substring(1);
                };
            })
            .filter('statusFilter', function () {
                return function (input) {
                    return input === 'A' ? 'Ativo' : 'Inativo';
                };
            }).filter('statusFilter', function () {
                return function (input) {

                    if (input === "RE") {
                        return 'Reserved';
                    }

                    if (input === "PA") {
                        return 'Paid';
                    }

                    if (input === "CA") {
                        return 'Canceled';
                    }
                };
            })
            .filter('simNaoFilter', function () {
                return function (input) {
                    return input === 'S' ? 'Sim' : 'Não';
                };
            })
            .filter('asDate', function () {
                return function (input) {
                    if (input == undefined)
                        return '';
                    var dt = input;
                    return new Date(dt);
                };
            }).filter('noArray', function ($filter) {
        return function (list, arrayFilter, element) {
            if (arrayFilter) {

                // Montando o array baseado nos IDs do filtro
                var arrKey = [];
                angular.forEach(arrayFilter, function (item) {
                    arrKey.push(item[element]);
                });

                return $filter("filter")(list, function (listItem) {

                    return (arrKey.length > 0) ? arrKey.indexOf(listItem[element]) === -1 : true;

                });
            }
        };
    });
})();