(function () {
    'use strict';

    angular.module('app.core')
            .factory('menuSrv', menuSrv);

    menuSrv.$inject = ['$window'];

    function menuSrv($window) {

        var factory = {
            menu: [],
            setMenu: set,
            getMenu: get
        };

        set();
        
        function get() {

            return factory.menu;
            
        }
        
        function set() {

            try {
//                var menu = angular.fromJson($window.sessionStorage.menu) || [];
//                factory.menu = menu.workflow;

                factory.menu = angular.fromJson($window.sessionStorage.menu) || [];

            } catch (e) {
                factory.menu = [];
            }
            
        }
        

        return factory;
    }
})();